function MidiFile(e) {
    function t(e) {
        var t = e.read(4),
            n = e.readInt32();
        return {
            id: t,
            length: n,
            data: e.read(n)
        }
    }

    function n(e) {
        var t, n, i, a, s, o, c = {};
        if (c.deltaTime = e.readVarInt(), t = e.readInt8(), 240 == (240 & t)) {
            if (255 == t) {
                switch (c.type = "meta", n = e.readInt8(), i = e.readVarInt(), n) {
                    case 0:
                        if (c.subtype = "sequenceNumber", 2 != i) throw "Expected length for sequenceNumber event is 2, got " + i;
                        return c.number = e.readInt16(), c;
                    case 1:
                        return c.subtype = "text", c.text = e.read(i), c;
                    case 2:
                        return c.subtype = "copyrightNotice", c.text = e.read(i), c;
                    case 3:
                        return c.subtype = "trackName", c.text = e.read(i), c;
                    case 4:
                        return c.subtype = "instrumentName", c.text = e.read(i), c;
                    case 5:
                        return c.subtype = "lyrics", c.text = e.read(i), c;
                    case 6:
                        return c.subtype = "marker", c.text = e.read(i), c;
                    case 7:
                        return c.subtype = "cuePoint", c.text = e.read(i), c;
                    case 32:
                        if (c.subtype = "midiChannelPrefix", 1 != i) throw "Expected length for midiChannelPrefix event is 1, got " + i;
                        return c.channel = e.readInt8(), c;
                    case 47:
                        if (c.subtype = "endOfTrack", 0 != i) throw "Expected length for endOfTrack event is 0, got " + i;
                        return c;
                    case 81:
                        if (c.subtype = "setTempo", 3 != i) throw "Expected length for setTempo event is 3, got " + i;
                        return c.microsecondsPerBeat = (e.readInt8() << 16) + (e.readInt8() << 8) + e.readInt8(), c;
                    case 84:
                        if (c.subtype = "smpteOffset", 5 != i) throw "Expected length for smpteOffset event is 5, got " + i;
                        return a = e.readInt8(), c.frameRate = {
                            0: 24,
                            32: 25,
                            64: 29,
                            96: 30
                        }[96 & a], c.hour = 31 & a, c.min = e.readInt8(), c.sec = e.readInt8(), c.frame = e.readInt8(), c.subframe = e.readInt8(), c;
                    case 88:
                        if (c.subtype = "timeSignature", 4 != i) throw "Expected length for timeSignature event is 4, got " + i;
                        return c.numerator = e.readInt8(), c.denominator = Math.pow(2, e.readInt8()), c.metronome = e.readInt8(), c.thirtyseconds = e.readInt8(), c;
                    case 89:
                        if (c.subtype = "keySignature", 2 != i) throw "Expected length for keySignature event is 2, got " + i;
                        return c.key = e.readInt8(!0), c.scale = e.readInt8(), c;
                    case 127:
                        return c.subtype = "sequencerSpecific", c.data = e.read(i), c;
                    default:
                        return c.subtype = "unknown", c.data = e.read(i), c
                }
                return c.data = e.read(i), c
            }
            if (240 == t) return c.type = "sysEx", i = e.readVarInt(), c.data = e.read(i), c;
            if (247 == t) return c.type = "dividedSysEx", i = e.readVarInt(), c.data = e.read(i), c;
            throw "Unrecognised MIDI event type byte: " + t
        }
        switch (0 == (128 & t) ? (s = t, t = r) : (s = e.readInt8(), r = t), o = t >> 4, c.channel = 15 & t, c.type = "channel", o) {
            case 8:
                return c.subtype = "noteOff", c.noteNumber = s, c.velocity = e.readInt8(), c;
            case 9:
                return c.noteNumber = s, c.velocity = e.readInt8(), 0 == c.velocity ? c.subtype = "noteOff" : c.subtype = "noteOn", c;
            case 10:
                return c.subtype = "noteAftertouch", c.noteNumber = s, c.amount = e.readInt8(), c;
            case 11:
                return c.subtype = "controller", c.controllerType = s, c.value = e.readInt8(), c;
            case 12:
                return c.subtype = "programChange", c.programNumber = s, c;
            case 13:
                return c.subtype = "channelAftertouch", c.amount = s, c;
            case 14:
                return c.subtype = "pitchBend", c.value = s + (e.readInt8() << 7), c;
            default:
                throw "Unrecognised MIDI event type: " + o
        }
    }
    var r, i, a, s, o, c, u, d, h, f, p, l;
    if (stream = Stream(e), i = t(stream), "MThd" != i.id || 6 != i.length) throw "Bad .mid file - header not found";
    if (a = Stream(i.data), s = a.readInt16(), o = a.readInt16(), c = a.readInt16(), 32768 & c) throw "Expressing time division in SMTPE frames is not supported yet";
    for (ticksPerBeat = c, u = {
            formatType: s,
            trackCount: o,
            ticksPerBeat: ticksPerBeat
        }, d = [], h = 0; h < u.trackCount; h++) {
        if (d[h] = [], f = t(stream), "MTrk" != f.id) throw "Unexpected chunk - expected MTrk, got " + f.id;
        for (p = Stream(f.data); !p.eof();) l = n(p), d[h].push(l)
    }
    return {
        header: u,
        tracks: d
    }
}

function Replayer(e, t, n, r) {
    function i() {
        var t, n, r = null,
            i = null,
            a = null;
        for (t = 0; t < d.length; t++) null != d[t].ticksToNextEvent && (null == r || d[t].ticksToNextEvent < r) && (r = d[t].ticksToNextEvent, i = t, a = d[t].nextEventIndex);
        if (null != i) {
            for (n = e.tracks[i][a], e.tracks[i][a + 1] ? d[i].ticksToNextEvent += e.tracks[i][a + 1].deltaTime : d[i].ticksToNextEvent = null, d[i].nextEventIndex += 1, t = 0; t < d.length; t++) null != d[t].ticksToNextEvent && (d[t].ticksToNextEvent -= r);
            return {
                ticksToEvent: r,
                event: n,
                track: i
            }
        }
        return null
    }

    function a() {
        function e() {
            var e, n, r;
            f || "meta" != c.event.type || "setTempo" != c.event.subtype || (h = 6e7 / c.event.microsecondsPerBeat), e = 0, n = 0, c.ticksToEvent > 0 && (e = c.ticksToEvent / p, n = e / (h / 60)), r = 1e3 * n * t || 0, u.push([c, r]), c = i()
        }
        if (c = i())
            for (; c;) e(!0)
    }
    var s, o, c, u, d = [],
        h = r ? r : 120,
        f = !!r,
        p = e.header.ticksPerBeat;
    for (s = 0; s < e.tracks.length; s++) d[s] = {
        nextEventIndex: 0,
        ticksToNextEvent: e.tracks[s].length ? e.tracks[s][0].deltaTime : null
    };
    return o = 0, u = [], a(), {
        getData: function() {
            return clone(u)
        }
    }
}

function Stream(e) {
    function t(t) {
        var n = e.substr(o, t);
        return o += t, n
    }

    function n() {
        var t = (e.charCodeAt(o) << 24) + (e.charCodeAt(o + 1) << 16) + (e.charCodeAt(o + 2) << 8) + e.charCodeAt(o + 3);
        return o += 4, t
    }

    function r() {
        var t = (e.charCodeAt(o) << 8) + e.charCodeAt(o + 1);
        return o += 2, t
    }

    function i(t) {
        var n = e.charCodeAt(o);
        return t && n > 127 && (n -= 256), o += 1, n
    }

    function a() {
        return o >= e.length
    }

    function s() {
        for (var e, t = 0;;) {
            if (e = i(), !(128 & e)) return t + e;
            t += 127 & e, t <<= 7
        }
    }
    var o = 0;
    return {
        eof: a,
        read: t,
        readInt32: n,
        readInt16: r,
        readInt8: i,
        readVarInt: s
    }
}
var Base64Binary, clone = function(e) {
    var t, n;
    if ("object" != typeof e) return e;
    if (null == e) return e;
    t = "number" == typeof e.length ? [] : {};
    for (n in e) t[n] = clone(e[n]);
    return t
};
! function() {
    function e(e) {
        this.message = e
    }
    var t = "undefined" != typeof exports ? exports : this,
        n = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    e.prototype = new Error, e.prototype.name = "InvalidCharacterError", t.btoa || (t.btoa = function(t) {
        for (var r, i, a = 0, s = n, o = ""; t.charAt(0 | a) || (s = "=", a % 1); o += s.charAt(63 & r >> 8 - a % 1 * 8)) {
            if (i = t.charCodeAt(a += .75), i > 255) throw new e("'btoa' failed: The string to be encoded contains characters outside of the Latin1 range.");
            r = r << 8 | i
        }
        return o
    }), t.atob || (t.atob = function(t) {
        if (t = t.replace(/=+$/, ""), t.length % 4 == 1) throw new e("'atob' failed: The string to be decoded is not correctly encoded.");
        for (var r, i, a = 0, s = 0, o = ""; i = t.charAt(s++); ~i && (r = a % 4 ? 64 * r + i : i, a++ % 4) ? o += String.fromCharCode(255 & r >> (-2 * a & 6)) : 0) i = n.indexOf(i);
        return o
    })
}(), Base64Binary = {
        _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
        decodeArrayBuffer: function(e) {
            var t = Math.ceil(3 * e.length / 4),
                n = new ArrayBuffer(t);
            return this.decode(e, n), n
        },
        decode: function(e, t) {
            var n, r, i, a, s, o, c, u, d, h, f = this._keyStr.indexOf(e.charAt(e.length - 1)),
                p = this._keyStr.indexOf(e.charAt(e.length - 1)),
                l = Math.ceil(3 * e.length / 4);
            for (64 == f && l--, 64 == p && l--, d = 0, h = 0, n = t ? new Uint8Array(t) : new Uint8Array(l), e = e.replace(/[^A-Za-z0-9\+\/\=]/g, ""), d = 0; d < l; d += 3) s = this._keyStr.indexOf(e.charAt(h++)), o = this._keyStr.indexOf(e.charAt(h++)), c = this._keyStr.indexOf(e.charAt(h++)), u = this._keyStr.indexOf(e.charAt(h++)), r = s << 2 | o >> 4, i = (15 & o) << 4 | c >> 2, a = (3 & c) << 6 | u, n[d] = r, 64 != c && (n[d + 1] = i), 64 != u && (n[d + 2] = a);
            return n
        }
    }, window.AudioContext = window.AudioContext || window.webkitAudioContext || null, window.OfflineAudioContext = window.OfflineAudioContext || window.webkitOfflineAudioContext || null,
    function(e) {
        var t, n, r, i = function(e) {
                return "[object Function]" === Object.prototype.toString.call(e) || "[object AudioContextConstructor]" === Object.prototype.toString.call(e)
            },
            a = [
                ["createGainNode", "createGain"],
                ["createDelayNode", "createDelay"],
                ["createJavaScriptNode", "createScriptProcessor"]
            ];
        i(e) && (n = new e, n.destination && n.sampleRate && (t = e.prototype, r = Object.getPrototypeOf(n.createBufferSource()), i(r.start) || i(r.noteOn) && (r.start = function(e, t, n) {
            switch (arguments.length) {
                case 0:
                    throw new Error("Not enough arguments.");
                case 1:
                    this.noteOn(e);
                    break;
                case 2:
                    if (!this.buffer) throw new Error("Missing AudioBuffer");
                    this.noteGrainOn(e, t, this.buffer.duration - t);
                    break;
                case 3:
                    this.noteGrainOn(e, t, n)
            }
        }), i(r.noteOn) || (r.noteOn = r.start), i(r.noteGrainOn) || (r.noteGrainOn = r.start), i(r.stop) || (r.stop = r.noteOff), i(r.noteOff) || (r.noteOff = r.stop), a.forEach(function(e) {
            for (var t, n; e.length;) t = e.pop(), i(this[t]) ? this[e.pop()] = this[t] : (n = e.pop(), this[t] = this[n])
        }, t)))
    }(window.AudioContext),
    function(e) {
        "use strict";

        function t() {}

        function n() {
            var e, t, n, r, i;
            this.inputInUse = !1, this.outputInUse = !1, e = document.createElement("object"), e.id = "_Jazz" + Math.random() + "ie", e.classid = "CLSID:1ACE1618-1C7D-4561-AEE1-34842AA85E90", this.activeX = e, t = document.createElement("object"), t.id = "_Jazz" + Math.random(), t.type = "audio/x-jazz", e.appendChild(t), this.objRef = t, n = document.createElement("p"), n.appendChild(document.createTextNode("This page requires the ")), r = document.createElement("a"), r.appendChild(document.createTextNode("Jazz plugin")), r.href = "http://jazz-soft.net/", n.appendChild(r), n.appendChild(document.createTextNode(".")), t.appendChild(n), i = document.getElementById("MIDIPlugin"), i || (i = document.createElement("div"), i.id = "MIDIPlugin", i.style.position = "absolute", i.style.visibility = "hidden", i.style.left = "-9999px", i.style.top = "-9999px", document.body.appendChild(i)), i.appendChild(e), this.objRef.isJazz ? this._Jazz = this.objRef : this.activeX.isJazz ? this._Jazz = this.activeX : this._Jazz = null, this._Jazz && (this._Jazz._jazzTimeZero = this._Jazz.Time(), this._Jazz._perfTimeZero = window.performance.now())
        }

        function r() {
            this._promise && this._promise.fail({
                code: 1
            })
        }

        function i() {
            this.jazz.MidiOutLong(this.data)
        }
        var a, s, o, c, u, d;
        t.prototype.then = function(e, t) {
            this.accept = e, this.reject = t
        }, t.prototype.succeed = function(e) {
            this.accept && this.accept(e)
        }, t.prototype.fail = function(e) {
            this.reject && this.reject(e)
        }, a = function() {
            var e = new s;
            return e._promise
        }, s = function() {
            this._jazzInstances = new Array, this._jazzInstances.push(new n), this._promise = new t, this._jazzInstances[0]._Jazz ? (this._Jazz = this._jazzInstances[0]._Jazz, window.setTimeout(o.bind(this), 3)) : window.setTimeout(r.bind(this), 3)
        }, o = function() {
            this._promise && this._promise.succeed(this)
        }, s.prototype.inputs = function() {
            var e, t, n;
            if (!this._Jazz) return null;
            for (e = this._Jazz.MidiInList(), t = new Array(e.length), n = 0; n < e.length; n++) t[n] = new c(this, e[n], n);
            return t
        }, s.prototype.outputs = function() {
            var e, t, n;
            if (!this._Jazz) return null;
            for (e = this._Jazz.MidiOutList(), t = new Array(e.length), n = 0; n < e.length; n++) t[n] = new u(this, e[n], n);
            return t
        }, c = function(e, t, r) {
            var i, a;
            for (this._listeners = [], this._midiAccess = e, this._index = r, this._inLongSysexMessage = !1, this._sysexBuffer = new Uint8Array, this.id = "" + r + "." + t, this.manufacturer = "", this.name = t, this.type = "input", this.version = "", this.onmidimessage = null, i = null, a = 0; a < e._jazzInstances.length && !i; a++) e._jazzInstances[a].inputInUse || (i = e._jazzInstances[a]);
            i || (i = new n, e._jazzInstances.push(i)), i.inputInUse = !0, this._jazzInstance = i._Jazz, this._input = this._jazzInstance.MidiInOpen(this._index, d.bind(this))
        }, c.prototype.addEventListener = function(e, t, n) {
            if ("midimessage" === e) {
                for (var r = 0; r < this._listeners.length; r++)
                    if (this._listeners[r] == t) return;
                this._listeners.push(t)
            }
        }, c.prototype.removeEventListener = function(e, t, n) {
            if ("midimessage" === e)
                for (var r = 0; r < this._listeners.length; r++)
                    if (this._listeners[r] == t) return void this._listeners.splice(r, 1)
        }, c.prototype.preventDefault = function() {
            this._pvtDef = !0
        }, c.prototype.dispatchEvent = function(e) {
            this._pvtDef = !1;
            for (var t = 0; t < this._listeners.length; t++) this._listeners[t].handleEvent ? this._listeners[t].handleEvent.bind(this)(e) : this._listeners[t].bind(this)(e);
            return this.onmidimessage && this.onmidimessage(e), this._pvtDef
        }, c.prototype.appendToSysexBuffer = function(e) {
            var t = this._sysexBuffer.length,
                n = new Uint8Array(t + e.length);
            n.set(this._sysexBuffer), n.set(e, t), this._sysexBuffer = n
        }, c.prototype.bufferLongSysex = function(e, t) {
            for (var n = t; n < e.length;) {
                if (247 == e[n]) return n++, this.appendToSysexBuffer(e.slice(t, n)), n;
                n++
            }
            return this.appendToSysexBuffer(e.slice(t, n)), this._inLongSysexMessage = !0, n
        }, d = function(e, t) {
            var n, r, i = 0,
                a = !1;
            for (n = 0; n < t.length; n += i) {
                if (this._inLongSysexMessage) {
                    if (n = this.bufferLongSysex(t, n), 247 != t[n - 1]) return;
                    a = !0
                } else switch (a = !1, 240 & t[n]) {
                    case 128:
                    case 144:
                    case 160:
                    case 176:
                    case 224:
                        i = 3;
                        break;
                    case 192:
                    case 208:
                        i = 2;
                        break;
                    case 240:
                        switch (t[n]) {
                            case 240:
                                if (n = this.bufferLongSysex(t, n), 247 != t[n - 1]) return;
                                a = !0;
                                break;
                            case 241:
                            case 243:
                                i = 2;
                                break;
                            case 242:
                                i = 3;
                                break;
                            default:
                                i = 1
                        }
                }
                r = document.createEvent("Event"), r.initEvent("midimessage", !1, !1), r.receivedTime = parseFloat(e.toString()) + this._jazzInstance._perfTimeZero, a || this._inLongSysexMessage ? (r.data = new Uint8Array(this._sysexBuffer), this._sysexBuffer = new Uint8Array(0), this._inLongSysexMessage = !1) : r.data = new Uint8Array(t.slice(n, i + n)), this.dispatchEvent(r)
            }
        }, u = function(e, t, r) {
            var i, a;
            for (this._listeners = [], this._midiAccess = e, this._index = r, this.id = "" + r + "." + t, this.manufacturer = "", this.name = t, this.type = "output", this.version = "", i = null, a = 0; a < e._jazzInstances.length && !i; a++) e._jazzInstances[a].outputInUse || (i = e._jazzInstances[a]);
            i || (i = new n, e._jazzInstances.push(i)), i.outputInUse = !0, this._jazzInstance = i._Jazz, this._jazzInstance.MidiOutOpen(this.name)
        }, u.prototype.send = function(e, t) {
            var n, r = 0;
            return 0 !== e.length && (t && (r = Math.floor(t - window.performance.now())), t && r > 1 ? (n = new Object, n.jazz = this._jazzInstance, n.data = e, window.setTimeout(i.bind(n), r)) : this._jazzInstance.MidiOutLong(e), !0)
        }, window.navigator.requestMIDIAccess || (window.navigator.requestMIDIAccess = a)
    }(window),
    function(e) {
        function t() {
            for (var t = ["moz", "webkit", "o", "ms"], n = t.length, r = {
                    value: function(e) {
                        return function() {
                            return Date.now() - e
                        }
                    }(Date.now())
                }; n >= 0; n--)
                if (t[n] + "Now" in e.performance) return r.value = function(t) {
                    return function() {
                        e.performance[t]()
                    }
                }(t[n] + "Now"), r;
            return "timing" in e.performance && "connectStart" in e.performance.timing && (r.value = function(e) {
                return function() {
                    Date.now() - e
                }
            }(e.performance.timing.connectStart)), r
        }
        var n, r = {};
        "performance" in e && "now" in e.performance || ("performance" in e || Object.defineProperty(e, "performance", {
            get: function() {
                return r
            }
        }), n = t(), Object.defineProperty(e.performance, "now", n))
    }(window);