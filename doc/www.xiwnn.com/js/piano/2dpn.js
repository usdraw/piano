"use strict";
var piano, basetime = (new Date).getTime(),
    currenttime = (new Date).getTime(),
    currentidx = 0,
    playseries = [],
    presslist = new Array(89),
    recorder = {};
! function(e) {
    function t() {}
    e.keypst = null, e.play = function() {
        currenttime = 0, currentidx = 0, basetime = (new Date).getTime()
    }, e.record = function(e, t, n) {
        return
    }, t()
}(recorder), piano = window.piano = {},
    function(e) {
        function t() {
            function t() {
                function e(e) {
                    null == e || (e > 0 ? w += .05769 : e < 0 && (w -= .05769)), w > 0 && (w = 0), w < k - 1 ? w = k - 1 : 0, h = f * w, N.css("left", h + "px"), W.css("left", -w * V.width() + "px")
                }

                function t(t) {
                    var n, o, i, r = q.width(),
                        s = q.height(),
                        a = r / g;
                    a = a < .7 ? .7 : a, a *= P, r > g * a && (n = r / g, P = n * P / a, a = n), N.css({
                        transform: "scale(" + a + ")"
                    }), o = q.offset(), c.left = o.left, c.top = o.top + q.height() - (N.css("bottom").replace("px", "") - 0), l = G * a, p = J * a, v = p / l, m = K * a, y = R * a, f = g * a, i = r / f, w -= (k - i) / 2, k = i, b = V.width() * k, e(), 1 == t ? (clearTimeout(X), X = setTimeout(function() {
                        V.css("bottom", y > s - 60 ? s - 30 : y + 30)
                    }, 800)) : V.css("bottom", y > s - 60 ? s - 30 : y + 30), W.width(b)
                }

                function s(e) {
                    return "js-kbd-zoom-in" == this.id ? P *= 1.2 : P /= 1.2, t(!0), e.preventDefault(), e.stopPropagation(), !1
                }

                function a(t) {
                    return e("js-kbd-mlef" == this.id ? 1 : -1), t.preventDefault(), t.stopPropagation(), !1
                }

                function d() {
                    var e = u(this).attr("data-m") - 0,
                        t = L + e;
                    t < -28 || t > 87 || (L = t, r())
                }
                var C, E, O, T, M, F, _, z, I, U, A, B, H, X, Y, q = u("#keycontent"),
                    N = u("#keyboard"),
                    Q = u("#keyboard>.black"),
                    S = u("#keyboard>.white"),
                    V = u("#slidescroll"),
                    W = u("#slidebar"),
                    G = 39,
                    J = 13,
                    K = 70,
                    R = 200;
                for (C = 1, E = 0, O = 0, T = 0, M = 0, F = 65, _ = -2, z = !0, I = !1; C < 89; C++) U = C < 10 ? "0" + C : C, 1 == z || I ? (z = !1, 1 == I && (I = !1, z = !0), A = _ <= -1 ? "<sub>" + Math.abs(_) + "</sub>" : _ >= 2 ? "<sup>" + (_ - 1) + "</sup>" : "", B = u("<div id='key_" + U + "' class='key' >" + C + " <div>&#" + (_ < 1 ? F : F + 32) + ";" + A + "</div></div>"), S.append(B), n[C] = B, j[C] = G * M + 20, 71 == F ? F = 64 : 0, 66 == F ? _++ : 0, x[M] = C, M++, F++) : (B = u("<div id='key_" + U + "' class='key' >" + C + "</div>"), B.css("left", G * O + "px"), Q.append(B), n[C] = B, j[C] = G * O + 40, 0 != E && 2 != T && 5 != T || (O++, I = !0, 5 == T ? T = 0 : 0), x[M - 1 + "+"] = C, x[M + "-"] = C, E++, O++, T++, z = !0);
                H = 0, X = -1, t(), u(window).resize(t), u("#js-kbd-zoom-in,#js-kbd-zoom-out").mousedown(s).on("touchstart", s), u("#js-kbd-mlef,#js-kbd-mrit").mousedown(a).on("touchstart", a), Y = "";
                for (C in D) Y += "<i id='keycode_" + C + "' >" + C.toUpperCase() + "</i>";
                u(".key-maps").html(Y), r(), u(".js-key-move").mousedown(d).on("touchstart", d), o(), i()
            }

            function o() {
                function t(t) {
                    var n, o, i;
                    if (t != j) {
                        for (n = t.split("@"), o = j.split("@"), i = 1; i < n.length; i++) "" == n[i] || "" != j && j.indexOf(n[i]) != -1 || e.keyDown(n[i] - 0);
                        for (i = 1; i < o.length; i++) "" == o[i] || "" != t && t.indexOf(o[i]) != -1 || e.keyUp(o[i] - 0);
                        j = t
                    }
                }

                function n(e, t) {
                    var n, o, i, r, s = e - c.left,
                        a = c.top - t;
                    if (a < 0 || a >= y) return "";
                    if (n = s - h, o = a, i = n / l, r = Math.floor(i), o >= m)
                        if (i - r < v) {
                            if (void 0 != x[r + "-"]) return "@" + x[r + "-"]
                        } else if (r + 1 - i < v && void 0 != x[r + "+"]) return "@" + x[r + "+"];
                    return void 0 != x[r] ? "@" + x[r] : ""
                }

                function o(e) {
                    var t, o = e.touches,
                        i = "";
                    for (t = 0; t < o.length; t++) i += n(o[t].pageX, o[t].pageY);
                    return i
                }

                function i(e) {
                    e.preventDefault(), e.stopPropagation(), t(o(e)), D = !0
                }

                function r(e) {
                    e.preventDefault(), e.stopPropagation(), t(o(e))
                }

                function s(e) {
                    e.preventDefault(), e.stopPropagation(), t(o(e))
                }

                function a(e) {
                    return n(e.pageX, e.pageY)
                }

                function d(e) {
                    return D ? (document.removeEventListener("mousedown", d, !1), b.removeEventListener("mousemove", f, !1), void document.removeEventListener("mouseup", p, !1)) : (C = !0, e.preventDefault(), e.stopPropagation(), void t(a(e)))
                }

                function f(e) {
                    D || C && (e.preventDefault(), e.stopPropagation(), t(a(e)))
                }

                function p(e) {
                    D || (C = !1, t(""))
                }

                function w(e) {
                    t("")
                }
                var g, k = u(".keyboard"),
                    b = document.getElementById("keyboard"),
                    P = document.getElementById("content"),
                    j = "",
                    D = !1,
                    C = !1;
                k.mouseleave(w), g = "", P.addEventListener("mousedown", d, !1), b.addEventListener("mousemove", f, !1), document.addEventListener("mouseup", p, !1), P.addEventListener("touchstart", i, !1), P.addEventListener("touchend", r, !1), P.addEventListener("touchmove", s, !1)
            }

            function i() {
                var t, n = {};
                for (t in C) n[t] = !1;
                document.onkeydown = function(t) {
                    if (void 0 !== n[t.which] && !n[t.which]) {
                        n[t.which] = !0;
                        var o = C[t.which] + L;
                        if (o < 1 || o > 88) return;
                        e.keyDown(o)
                    }
                }, document.onkeyup = function(t) {
                    if (void 0 !== n[t.which] && n[t.which]) {
                        n[t.which] = !1;
                        var o = C[t.which] + L;
                        if (o < 1 || o > 88) return;
                        e.keyUp(o)
                    }
                }
            }

            function r() {
                u(".key-maps>i").each(function(e, t) {
                    var n = t.id.replace("keycode_", ""),
                        o = D[n] - 0 + L;
                    o > 0 && o < 89 ? t.style.left = j[o] + "px" : o <= 0 ? t.style.left = "-10px" : t.style.left = "100%"
                })
            }

            function s() {
                a.loadPlugin({
                    soundfontUrl: "./js/piano/soundfont/",
                    instrument: "acoustic_grand_piano",
                    onprogress: function(e, t) {},
                    onsuccess: function() {
                        function e(e) {
                            var t = e / 60 >> 0,
                                n = String(e - 60 * t >> 0);
                            return 1 == n.length && (n = "0" + n), t + ":" + n
                        }
                        var t, o, i;
                        u("#js-loader-tips").removeClass("ing"), setTimeout(function() {
                            u("#js-loader-tips").remove()
                        }, 1e3), a.setVolume(0, 127), a.Player.addListener(function(e) {
                            var t = e.note - 20;
                            n[t] && (144 === e.message ? n[t].addClass("press") : n[t].removeClass("press"))
                        }), t = u("div.mp-pblefc")[0], o = u("span.mp-tnow")[0], i = u("span.mp-ttotal")[0], a.Player.setAnimation(function(n, r) {
                            var s = n.now / n.end,
                                d = n.now >> 0,
                                c = n.end >> 0;
                            d === c && (a.Player.stop(), u("div.mp-play").removeClass("playing")), t.style.width = 100 * s + "%", o.innerHTML = e(d), i.innerHTML = e(c)
                        }), d()
                    }
                }), u("#js-loader-tips").addClass("show")
            }

            function d() {
                var t, n, o;
                a.Player.timeWarp = 1, t = window.location.hash.split("&"), n = "", o = "", t.length > 0 && "" != t[0] && "#" != t[0] && (n = "/piano/getmidifile/" + t[0].replace("#", ""), t.length > 1 && (t[1] + "").indexOf("n=") != -1 && (o = (t[1] + "").replace("n=", ""))), "" == n ? u(".mp-huange").text("选曲") : e.readyFile(n, o, !0, function() {
                    var t, n;
                    if ("" == o) {
                        t = e.getFileMeta();
                        for (n in t)
                            if ("trackName" == t[n].subtype) return void u("#js-namespan").text(t[n].text)
                    }
                }, function() {
                    console.log("midi文件加载错误")
                })
            }
            var c = {
                    left: 0,
                    top: 0
                },
                l = 39,
                f = 1400,
                p = 13,
                v = .33333,
                m = 70,
                y = 200,
                h = 0,
                w = 0,
                g = u("#keyboard").width(),
                k = 1,
                b = 100,
                P = 1,
                x = {},
                j = {},
                D = {
                    z: 1,
                    s: 2,
                    x: 3,
                    d: 4,
                    c: 5,
                    v: 6,
                    g: 7,
                    b: 8,
                    h: 9,
                    n: 10,
                    j: 11,
                    m: 12,
                    q: 13,
                    2: 14,
                    w: 15,
                    3: 16,
                    e: 17,
                    r: 18,
                    5: 19,
                    t: 20,
                    6: 21,
                    y: 22,
                    7: 23,
                    u: 24,
                    i: 25,
                    9: 26,
                    o: 27,
                    0: 28,
                    p: 29
                },
                C = {
                    90: 1,
                    83: 2,
                    88: 3,
                    68: 4,
                    67: 5,
                    86: 6,
                    71: 7,
                    66: 8,
                    72: 9,
                    78: 10,
                    74: 11,
                    77: 12,
                    188: 13,
                    81: 13,
                    76: 14,
                    50: 14,
                    190: 15,
                    87: 15,
                    186: 16,
                    51: 16,
                    191: 17,
                    69: 17,
                    82: 18,
                    53: 19,
                    84: 20,
                    54: 21,
                    89: 22,
                    55: 23,
                    85: 24,
                    73: 25,
                    57: 26,
                    79: 27,
                    48: 28,
                    80: 29,
                    219: 30,
                    187: 31,
                    221: 32
                },
                L = 27;
            recorder.keypst = j, t(), s()
        }
        var n, o, i, r, s, a = window.MIDI,
            u = window.jQuery;
        for (e.presskeys = new Array(89), n = {}, window.keys = n, o = 1; o <= 88; o++) e.presskeys[o] = !1;
        i = !1, s = u("span#js-namespan")[0], t(), e.keyDown = function(t, o) {
            a.noteOff(0, t + 20, 0), a.noteOn(0, t + 20, 50, 0), recorder.record(t, !0, 50), e.presskeys[t] = !0, n[t].addClass("press")
        }, e.keyUp = function(t) {
            e.presskeys[t] = !1, i || (a.noteOff(0, t + 20, 0), recorder.record(t, !1)), n[t].removeClass("press")
        }, e.sustainOn = function() {
            i = !0
        }, e.sustainOff = function() {
            i = !1;
            for (var t in e.presskeys) e.presskeys[t] || (a.noteOff(0, parseInt(t) + 20, 0), recorder.record(t, !1))
        }, e.readyFile = function(t, n, o, i, r) {
            return e.stop(), void 0 == t ? (console.error("midi路径为空，无法播放！"), void alert("midi路径为空，无法播放！")) : (void 0 == n && (n = "未命名文件"), u(s).text(n), void(1 == o ? a.Player.loadFile(t, function() {
                e.start()
            }, i, r) : a.Player.loadFile(t, null, i, r)))
        }, e.getFileMeta = function() {
            var e, t, n, o, i, r;
            if (void 0 != a.Player && void 0 != a.Player.data) {
                for (e = a.Player.data, t = [], n = 0, r = 0; r < e.length; r++)
                    if (o = e[r], o[0].event)
                        if (i = o[0].event, "meta" == i.type) t.push(i);
                        else if (n++, n > 5) break;
                return t
            }
            return []
        }, e.pause = function() {
            u(r).hasClass("playing") && (u(r).removeClass("playing"), a.Player.pause())
        }, e.start = function() {
            u(r).hasClass("playing") || u(r).addClass("playing"), a.Player.start()
        }, e.stop = function() {
            a.Player.stop(), u(r).removeClass("playing")
        }
    }(piano);